import os
import json
import preprocessing
import dataprocessing
import csv
import datetime, time
# from preprocessing import *
# from dataprocessing import *

dirname = os.path.dirname(__file__)
path_to_location_data = os.path.join(dirname, 'data/LocationHistory.json')
path_to_save_data = os.path.join(dirname, 'data/ProcessedTakeoutData.json')
path_to_csv_file = os.path.join(dirname, 'data/e1.csv')
path_to_tokyo_data = os.path.join(dirname, 'data/TokyoData.json')

# Array to store all status segments needed for activity and trip identification
data_segments = []

# Load data from specified json file
def load_data(path):
    with open(path, 'r') as f:
        location_dict = json.load(f)
    return location_dict


# Save pruned data to test visualisation to ensure all unnecessary points have been pruned
def save_data(data, path):
    with open(path, 'w') as outfile:
        json.dump(data, outfile)

def load_csv():
    data ={}
    with open(path_to_csv_file, 'r') as f:
        csv_reader = csv.DictReader(f, delimiter=',')
        line_count = 0
        for row in csv_reader:
            key = row['userid']
            row['timestamp'] = time.mktime(datetime.datetime.strptime(row['timestamp'], "%y/%m/%d %H:%M").timetuple())
            item = {'userid': row['userid'],
                    'timestamp': row['timestamp'],
                    'latitude': row['latitude'],
                    'longitude': row['longitude'],
                    'mode': row['mode'],
                    'magnification': row['magnification']}
            data.setdefault(key, []).append(item)
    return data

def main():
    # Load the data from the json file
    # json_data = load_data(path_to_location_data)
    # data = json_data['locations']
    # count = len(data)
    # print('count before pruning: (%d)' % count)

    # Load csv file
    data = load_data(path_to_tokyo_data)

    # # Pre-process google takeout data
    # preprocessing.convert_latlng(data)
    # preprocessing.remove_low_accuracy_data(data)
    # preprocessing.remove_out_of_bounds(data)
    # # Check all dat is in chronological order
    # preprocessing.check_in_order(data)

    # Process the location data by identifying activities and trips
    processed_data = dataprocessing.process_data(data)

    save_data(processed_data, path_to_save_data)


main()



