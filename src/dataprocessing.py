import mpu
import copy

# NOTE: For the following process it is necessary for the data to be in chronological order

# threshold use to determine the status of a data segment
v_threshold = 0.5 # meters per second
# threshold used to decide if a stationary segment needs to be adjusted
T_threshold = 30 # seconds
# threshold used to decide if a moving segment needs to be adjusted
D_threshold = 100 # meters
# threshold used to decide if an activity is a short time stop (such as traffic stops) or an activity
A_threshold = 300 # seconds


# identify and set status to either moving or stationary for each data_segment
# 1 represents moving segment
# 0 represents stationary segment
def identify_status(segment):
    if segment['average_speed'] > v_threshold:
        segment['status'] = 1 # segment is moving
    else:
        segment['status'] = 0 # segment is stationary
    return segment


# Create status segments using location data
# average_speed is in meters per second
# distance is in meters
def convert_to_data_segments(data):
    data_segments = []
    data_length = len(data)
    for i, location in enumerate(data):
        if i < data_length - 1:
            lat1 = location['latitude']
            lng1 = location['longitude']

            lat2 = data[i+1]['latitude']
            lng2 = data[i+1]['longitude']

            # mpu.harvesine_distance returns distance in km by default
            distance = mpu.haversine_distance((lat1, lng1), (lat2, lng2)) * 1000 # Convert to meters

            # in google takeout time is in milliseconds by default, convert to seconds to calculate meters per second
            duration = (int(data[i+1]['timestamp']) - int(location['timestamp']))
            average_speed = distance / duration

            segment = {
                'index': i,
                'location1': location,
                'location2': data[i+1],
                'distance': distance,
                'average_speed': average_speed,
                'duration': duration
            }
            # Identify status (moving or stationary) of created segment
            segment = identify_status(segment)
            data_segments.append(segment)

    return data_segments


def merge_adjacent_data_segments(data_segments):
    merged_segments = []
    merged_segment = {'locations': [],
                      'distance': 0,
                      'duration': 0,
                      'status': None}
    for i, segment in enumerate(data_segments):
        if i+1 < len(data_segments) and data_segments[i+1]['status'] == segment['status']:
            merged_segment['locations'].append(segment['location1'])
            merged_segment['distance'] += segment['distance']
            merged_segment['duration'] += segment['duration']
        else:
            merged_segment['locations'].append(segment['location1'])
            merged_segment['locations'].append(segment['location2'])
            merged_segment['distance'] += segment['distance']
            merged_segment['duration'] += segment['duration']
            merged_segment['status'] = segment['status']
            merged_segments.append(merged_segment)
            merged_segment = {'locations': [],
                              'distance': 0,
                              'duration': 0}

    return merged_segments

# Adjust status of each segment according to defined thresholds
def adjust_status(merged_segments):
    for segment in merged_segments:
        # If moving segment distance threshold not enough change to stationary
        if segment['status'] == 1 and segment['distance'] < D_threshold:
            segment['status'] = 0
        # If stationary segment not long enough convert to moving segment
        if segment['status'] == 0 and segment['duration'] < T_threshold:
            segment['status'] = 0
    return merged_segments


# Check all adjacent segments have different status
def check_segments(segments):
    for i, segment in enumerate(segments):
        if i + 1 < len(segments) and segment['status'] == segments[i+1]['status']:
            print("2 adjacent segments are the same")


# After adjusting status remerge adjacent segments with the same status
def final_merge_status_segments(status_segments):
    i = 0
    # j = len(status_segments[1]['locations'])
    while i + 1 < len(status_segments):
        if status_segments[i]['status'] == status_segments[i+1]['status']:
            status_segments[i]['locations'].extend(status_segments[i+1]['locations'][1:len(status_segments[i+1]['locations'])])
            status_segments[i]['distance'] += status_segments[i+1]['distance']
            status_segments[i]['duration'] += status_segments[i+1]['duration']
            del status_segments[i+1]
        else:
            i += 1
    return status_segments


# Identify short activities and regular activities based on a threshold
def identify_activity_types(segments):
    for segment in segments:
        if segment['status'] == 0:
            if segment['duration'] < 300:
                segment['stop_type'] = 'short'
            else: segment['stop_type'] = 'activity'

    return segments


# Create and return a new location for the filler segment used to indicate
# period between changes of transport modes
def get_filler_location(location):
    new_location = copy.deepcopy(location)
    # new_location["timestamp"] -= 30
    new_location["mode"] = "99"
    return new_location


def calculate_distance_and_duration(segment):
    distance = 0
    duration = segment["locations"][-1]["timestamp"] - segment["locations"][0]["timestamp"]
    for i, location in enumerate(segment["locations"]):
        if i + 1 < len(segment["locations"]):
            lat1 = location['latitude']
            lng1 = location['longitude']
            lat2 = segment["locations"][i + 1]['latitude']
            lng2 = segment["locations"][i + 1]['longitude']
            distance += mpu.haversine_distance((lat1, lng1), (lat2, lng2)) * 1000  # Convert to meters
    segment["distance"] = distance
    segment["duration"] = duration
    return segment


# Create new segment to indicate period between changes of transport modes (This is not reflected in the tokyo data)
def create_filler_segment(location):
    filler_location = get_filler_location(location)
    filler_segment = {"locations": [filler_location, location],
                   "status": 0,
                   "distance": 0,
                   "duration": 0,
                   "stop_type": "activity"}
    filler_segment = calculate_distance_and_duration(filler_segment)
    return filler_segment


# Create a new segment that splits a trip that uses multiple transport modes to ensure
# each trip has only one transport mode
def create_new_segment(locations):
    new_locations = copy.deepcopy(locations)
    new_segment = {"locations": new_locations,
                   "status": 1,
                   "distance": 0,
                   "duration": 0}
    new_segment = calculate_distance_and_duration(new_segment)
    return new_segment



def process_data(data):
    final_data = {}
    for key in data.keys():
        data_segments = convert_to_data_segments(data[key])
        merged_segments = merge_adjacent_data_segments(data_segments)
        merged_segments = adjust_status(merged_segments)
        final_segments = final_merge_status_segments(merged_segments)
        for j, segment in enumerate(final_segments):
            if(segment["status"] == 1):
                for i, location in enumerate(segment["locations"]):
                        if i + 2 < len(segment["locations"]) and i != 0 and segment["locations"][i+1]["mode"] != "99" \
                                and location["mode"] != segment["locations"][i+1]["mode"]:
                            filler_segment = create_filler_segment(segment["locations"][i+1])
                            final_segments.insert(j+1, filler_segment)
                            new_segment = create_new_segment(segment["locations"][i+1:])
                            del segment["locations"][i+2:]
                            final_segments.insert(j+2, new_segment)
                            segment = calculate_distance_and_duration(segment)
                            pass
        final_data[key] = identify_activity_types(final_segments)
    return final_data




