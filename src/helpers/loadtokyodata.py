import csv
import os
import json
import datetime, time


dirname = os.path.dirname(__file__)
path_to_csv_file = os.path.join(dirname, '../../data/e1.csv')
path_to_save_data = os.path.join(dirname, '../../data/TokyoData.json')





def load_csv():
    data ={}
    with open(path_to_csv_file, 'r') as f:
        csv_reader = csv.DictReader(f, delimiter=',')
        line_count = 0
        for row in csv_reader:
            key = row['userid']
            row['timestamp'] = time.mktime(datetime.datetime.strptime(row['timestamp'], "%y/%m/%d %H:%M").timetuple())
            item = {'userid': row['userid'],
                    'timestamp': row['timestamp'],
                    'latitude': float(row['longitude']),
                    'longitude': float(row['latitude']),
                    'mode': row['mode'],
                    'magnification': row['magnification']}
            data.setdefault(key, []).append(item)
    return data

# Save pruned data to test visualisation to ensure all unnecessary points have been pruned
def save_data(data, path):
    # pruned_data = {
    #     'data': data
    # }
    with open(path, 'w') as outfile:
        json.dump(data, outfile)

data = load_csv()
save_data(data, path_to_save_data)