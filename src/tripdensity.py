# This following code determines average locations of activities for visualisation of trip densities.
import os
import json
import mpu
import numpy

dirname = os.path.dirname(__file__)
path_to_processed_data = os.path.join(dirname, '../data/ProcessedGoogleData.json')


# Load data from specified json file
def load_data(path):
    with open(path, 'r') as f:
        location_dict = json.load(f)
    return location_dict


# Save pruned data to test visualisation to ensure all unnecessary points have been pruned
def save_data(data, path):
    with open(path, 'w') as outfile:
        json.dump(data, outfile)


def calculate_mean_locations(data):
    averages = []
    for i, segment in enumerate(data["data"]):
        if segment["status"] == 0:
            latitudes = [location["latitude"] for location in segment["locations"]]
            longitudes = [location["longitude"] for location in segment["locations"]]
            lat_avg = sum(latitudes)/len(latitudes)
            lng_avg = sum(longitudes)/len(longitudes)
            trip_ends = [data["data"][i-2]["id"]] if i - 2 >= 0 else []
            trip_starts = [data["data"][i+2]["id"]] if i + 2 < len(data["data"]) else []
            new_segment = {"latitude": lat_avg,
                           "longitude": lng_avg,
                           # Trip endpoints for trips where the current activity is a start point.
                           "trip_to": trip_starts,
                           # Trip start points for trips where the current activity is an endpoint
                           "trip_from": trip_ends,
                           "contains": [segment["id"]]}
            averages.append(new_segment)
    return averages

# Merge activity locations that occur close to each other
def merge_activities(data):
    for j, segment in enumerate(data):
        # Indices of locations to be merged to current segment. All activities within 500m of eachother are merged.
        locations_to_merge = [k for k, x in enumerate(data) if mpu.haversine_distance((segment["latitude"], segment["longitude"]),
                                                                                      (x["latitude"], x["longitude"])) * 1000 <= 100 and k != j]
        locations_to_merge.sort()
        removed = 0
        for i in locations_to_merge:
            i = i - removed
            segment["latitude"] = (segment["latitude"] + data[i]["latitude"]) / 2
            segment["longitude"] =(segment["longitude"] + data[i]["longitude"]) / 2
            segment["trip_to"] = segment["trip_to"] + data[i]["trip_to"]
            segment["trip_from"] = segment["trip_from"] + data[i]["trip_from"]
            segment["contains"] = segment["contains"] + data[i]["contains"]
            del data[i]
            removed += 1
    data = assign_activity_ids(data)
    return data


# Assign new ids to the newly merged activities
def assign_activity_ids(activities):
    for i, activity in enumerate(activities):
        activity["id"] = i
    return activities

# Using the processed data determine the od matrix
def determine_od_matrix(activities):
    od_matrix = numpy.zeros((len(activities), len(activities)), dtype=int)
    for activity in activities:
        i = activity["id"]
        for trip in activity["trip_to"]:
            j = next((a["id"] for a in activities if trip in a["contains"]), None)
            od_matrix[i][j] += 1
    return od_matrix

def activity_test(data):
    for i, activity in enumerate(data):
        for trip in activity["trip_to"]:
            if trip in activity["contains"]:
                print("This activity contains the thing that you dont want")


# Assign and id for each trip and each activity
def assign_ids(data):
    a = 0
    t = 0
    for segment in data:
        if segment["status"] == 1:
            id_t = "T"+str(t)
            segment["id"] = id_t
            t+=1
        else:
            id_a = "A"+str(a)
            segment["id"] = id_a
            a+=1
    return data

def save_od_matrix(od_matrix):
    numpy.savetxt("../data/odmatrix.csv", od_matrix, fmt="%d", delimiter=",")

def check_activities(data):
    for item in data:
        print(item["latitude"], item["longitude"])


def main():
    data = load_data(path_to_processed_data)
    # data = assign_ids(data)
    averages = calculate_mean_locations(data)
    merged_activities = merge_activities(averages)
    # check_activities(merged_activities)
    OD_matrix = determine_od_matrix(merged_activities)
    save_od_matrix(OD_matrix)
    save_data(merged_activities, "../data/merged_activities.json")
    # activity_test(merged_activities)
    # mean_actvity_locations = calculate_mean_locations(data)
main()