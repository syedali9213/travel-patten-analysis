

# Lat and long ranges to prune data and keep data for New Zealand(This is just for testing purposes)
# These ranges define ranges for New Zealand. It can be changed for desired reigon.
lat_from = -33.7895
lat_to = -47.6535

long_to = 165.0990
long_from = 179.9250


# Remove locations with accuracy less than 800 (this is for google takeout data.)
def remove_low_accuracy_data(data):
    i = 0
    while i < len(data):
        if data[i]['accuracy'] > 800:
            del data[i]
        else:
            i += 1


# Remove all data points that do not lie within the defined reigon.
# Boundaries are defined using latitude and longitude
def remove_out_of_bounds(data):
    i = 0
    while i < len(data):
        if(data[i]['latitude'] > lat_from or data[i]["latitude"] < lat_to
           or data[i]['longitude'] < long_to or data[i]['longitude'] > long_from):
            del data[i]
        else:
            i += 1

# Check all location is in chronological order.
def check_in_order(data):
    for i, location in enumerate(data):
        if i + 1 < len(data):
            i = i + 1
        if location["timestampMs"] > data[i]['timestampMs']:
            print('data is not in order, please sort data before proceeding')


#Convert latitude and longitude values from E7 to E0
def convert_latlng(data):
    for location in data:
        location['latitude'] = location.pop('latitudeE7')/10000000
        location['longitude'] = location.pop('longitudeE7') / 10000000


class Error(Exception):
    pass


class IncorrectPrune(Error):
    pass


# Checks the accuracy of all the data after purging to ensure data has been purged correctly.
# def check_accuracy(data):
#     try:
#         for i, location in enumerate(data):
#             if location['accuracy'] > 800:
#                 raise IncorrectPrune
#     except IncorrectPrune:
#         print('loaction too inaccurate, purgin was done incorrectly')