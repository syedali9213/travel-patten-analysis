import json
import mpu
import pandas as pd

# # TensorFlow and tf.keras
# import tensorflow as tf
# from tensorflow import keras
# from tensorflow import feature_column
# from keras import layers, Sequential

import tensorflow as tf

from tensorflow import feature_column
from tensorflow.keras import layers
from sklearn.model_selection import train_test_split

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import train_test_split


path_to_training_data = "../../data/trainingdatatokyo.json"


def load_data(path="../../data/ProcessedData.json"):
    with open(path, 'r') as f:
        location_dict = json.load(f)
    return location_dict


def save_data(data, path="../../data/trainingdatatokyo.json"):
    with open(path, 'w') as outfile:
        json.dump(data, outfile)


# Create training data from processed data segments by calculating,
# average speed, maximum speed, distance and target
def initialise_training_data():
    data = load_data()
    training_data = []
    for key in data.keys():
        user_data = data[key]
        for segment in user_data:
            if segment["status"] == 1:
                max_speed = 0
                average_speed = segment["distance"] / segment["duration"]
                distance = segment["distance"]
                mode_count = {"99": 0, "1": 0, "2": 0, "3": 0, "4": 0}
                for i, location in enumerate(segment["locations"]):
                    if i + 1 < len(segment["locations"]):
                        mode_count[location["mode"]] += 1
                        speed = (mpu.haversine_distance((location["latitude"],
                                                         location["longitude"]),
                                                        (segment["locations"][i+1]["latitude"],
                                                        segment["locations"][i+1]["longitude"])) * 1000)\
                                /(segment["locations"][i+1]["timestamp"] - location["timestamp"])
                        if speed > max_speed:
                            index = i
                            max_speed = speed
                target = int(max(mode_count, key=mode_count.get))
                if target == 1 and max_speed > 16:
                    print(index, len(segment["locations"]), target)
                    pass
                if target == 99:
                    target = 0
                    pass
                features = {"average_speed": average_speed,
                            "max_speed": max_speed,
                            "distance": distance,
                            "target": target}
                if target != 1:
                    training_data.append(features)
    save_data(training_data)


def df_to_dataset(dataframe, shuffle=True, batch_size=32):
    dataframe = dataframe.copy()
    labels = dataframe.pop('target')
    print(labels)
    ds = tf.data.Dataset.from_tensor_slices((dict(dataframe), labels))
    if shuffle:
        ds = ds.shuffle(buffer_size=len(dataframe))
    ds = ds.batch(batch_size)
    return ds


def train():
    tokyo_data = load_data(path_to_training_data)
    df = pd.DataFrame.from_dict(tokyo_data, orient='columns')
    train, test = train_test_split(df, test_size=0.2)
    train, val = train_test_split(train, test_size=0.2)

    feature_columns = []

    for header in ['average_speed', 'max_speed', 'distance']:
        feature_columns.append(feature_column.numeric_column(header))

    feature_layer = tf.keras.layers.DenseFeatures(feature_columns)

    batch_size = 32  # A small batch sized is used for demonstration purposes
    train_ds = df_to_dataset(train, batch_size=batch_size)
    val_ds = df_to_dataset(val, shuffle=False, batch_size=batch_size)
    test_ds = df_to_dataset(test, shuffle=False, batch_size=batch_size)

    for feature_batch, label_batch in train_ds.take(1):
        print('Every feature:', list(feature_batch.keys()))
        print('A batch of average_speed:', feature_batch['average_speed'])
        print('A batch of targets:', label_batch)

    model = tf.keras.Sequential([
        feature_layer,
        layers.Dense(6, activation='relu'),
        layers.Dense(5, activation='softmax')
    ])

    model.compile(optimizer="adam",
                  loss="sparse_categorical_crossentropy",
                  metrics=['accuracy',
                           "categorical_accuracy",
                           "sparse_categorical_accuracy"
                           ])

    model.fit(train_ds,
              validation_data=val_ds,
              epochs=10)

    loss, accuracy = model.evaluate(test_ds)
    print("Accuracy:", accuracy)


def main():
    initialise_training_data()
    # train()



main()